# Appendix #

## Description ##

The file ‘sys_data.mat’ contains the data-gernerating system, identification, and validation data set for the journal article P.B. Cox, R. Tóth, and M. Petreczky, “Towards Efficient Maximum Likelihood Estimation of LPV-SS Models”, Automatica, accepted for publication.

## Citation information ##

If you find any of the published documents useful, please cite it in your publication as
follows:

```
@Article{18Cox,
  author =       {Cox, P.B., Tóth, R., and Petreczky, M.},
  title =        {Towards Efficient Maximum Likelihood Estimation of LPV-SS Models},
  journal =      {Automatica, accepted for publication},
  year =         20118,
  volume =       {x},
  number =       {x},
  pages =        {x},
}
```

Rotterdam, The Netherlands, May 28, 2018
