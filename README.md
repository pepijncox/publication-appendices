# Digital Appendices for Publications #

## Description ##

This repository contains digital appendices of certain conference and journal papers I have contributed to. The files of one journal paper are combined into one folder. The various folders can be found in [this master directory](https://gitlab.com/pepijncox/publication-appendices/).

## Overview ##

The table below shows a list of directories containing digital appendices and its corresponding article

Directory | Bibliography
------------- | -------------
Towards Efficient Maximum - LPV-SS Models | P.B. Cox, R. Tóth, and M. Petreczky, “Towards Efficient Maximum Likelihood Estimation of LPV-SS Models”, Automatica, accepted for publication.

## Licensing ##

The published files and documents are made available under the GNU General Public License version 3, see [www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Citation information ##
If you find any of the published documents useful, please cite the conference or journal corresponding to these/that file(s).

## Acknowledgement ##

I would like to thank [Roland Tóth](http://rolandtoth.eu/) and [Mihály Petreczky](https://sites.google.com/site/mihalypetreczky/) for their contributions.
